import datetime

from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from airflow.models import Variable

from db.models import RandomData
from db.session import get_db
import requests


args = {
    'owner': 'Rustam',
    'provide_context':True
}


def extract_data(**kwargs):
    response = requests.get(
        'https://random-data-api.com/api/cannabis/random_cannabis?size=10'
    )

    if response.status_code == 200:
        json_data = response.json()
        kwargs['ti'].xcom_push(key='random_data_json', value=json_data)


def load_data(**kwargs):
    ti = kwargs['ti']
    json_data = ti.xcom_pull(key='random_data_json', task_ids=['extract'])[0]
    db = get_db().__next__()
    for row in json_data:
        db.add(RandomData(id=row['id'], 
                          uid=row['uid'], 
                          strain=row['strain'], 
                          cannabinoid_abbreviation=row['cannabinoid_abbreviation'], 
                          cannabinoid=row['cannabinoid'], 
                          terpene=row['terpene'], 
                          medical_use=row['medical_use'], 
                          health_benefit=row['health_benefit'], 
                          category=row['category'], 
                          type=row['type'], 
                          buzzword=row['buzzword'], 
                          brand=row['brand']))
        db.commit()
    print('data pushed')


with DAG(
    dag_id="random_data_etl",
    schedule_interval="0 */12 * * *",
    start_date=days_ago(2),
    default_args=args
) as dag:
    
    extract = PythonOperator(
        task_id="extract",
        python_callable=extract_data
        )
    
    loading = PythonOperator(
        task_id='loading',
        python_callable=load_data
    )

    extract >> loading