import os
from typing import Generator
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine


HOST = "host.docker.internal"
USER = os.getenv("POSTGRES_USER")
PASSWORD = os.getenv("POSTGRES_PASSWORD")
DB = os.getenv("POSTGRES_DB")
PORT = 9191

engine = create_engine(f"postgresql://{USER}:{PASSWORD}@{HOST}:{PORT}/{DB}")
Session = sessionmaker(bind=engine)


def get_db() -> Generator:
    with Session.begin() as db:
        yield db
