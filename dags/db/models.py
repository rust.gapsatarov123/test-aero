from sqlalchemy import (
    Column,
    Integer,
    TEXT,
    create_engine
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func
from dotenv import load_dotenv
import os

Base = declarative_base()

class RandomData(Base):
    __tablename__ = 'random_data'

    id = Column(Integer, primary_key=True, index=True)
    uid = Column(TEXT, comment="uid")
    strain = Column(TEXT, comment='напряжение')
    cannabinoid_abbreviation = Column(TEXT, comment='аббревеатура')
    cannabinoid = Column(TEXT, comment='каннабиноид')
    terpene = Column(TEXT, comment='терпен')
    medical_use = Column(TEXT, comment='медицинское использование')
    health_benefit = Column(TEXT, comment='польза для здоровья')
    category = Column(TEXT, comment='категория')
    type = Column(TEXT, comment='тип')
    buzzword = Column(TEXT, comment='модное слово')
    brand = Column(TEXT, comment='бренд')


if __name__ == '__main__':
    load_dotenv()

    engine = create_engine(f'postgresql://{os.getenv("POSTGRES_USER")}:{os.getenv("POSTGRES_PASSWORD")}@' +
                           f'localhost:{os.getenv("pgsql_port")}/{os.getenv("POSTGRES_DB")}')
    Base.metadata.create_all(engine)

